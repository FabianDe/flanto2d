function [D,nd] = cc_biharmonic(N,varargin)
%CC_BIHARMONIC Return the clamped-clamped biharmonic operator on the unit
%              interval
%
% USAGE: A = cc_biharmonic(N)
%
% INPUTS:
%    N = [Nx, Ny] : number of points in (x,y) directions.  Solve 1-D
%                   problem if length(N) = 1 or N(2) = 1
%
% OUTPUTS:
%    A = discretized biharmonic operator of size product(N) squared
%
% Initializations
D = [];
prod_N = prod(N);
debug = 1;

% Get number of dimensions
nd = length(N);
if (nd > 1 & N(2) == 1) 
    nd = 1;
end

% Debug output
if (debug)
    fprintf(1,'cc_biharmonic: nd = %d\n', nd);
end

% 1-d case
if (nd == 1)

    % spacing
    h = 1 ./ (N(1:nd)-1);
    if (debug)
        fprintf(1,'cc_biharmonic: h = %.8f\n', h);
    end
    h4inv = 1 / h^4;

    % preallocate operator
    D = repmat(0,[prod_N prod_N]);

    % simplify
    N1 = N(1);

    % set up Pade scheme with explicit FD at boundary using ghost cells
    alpha = 1/4;
    a = 3/2;
    alpha = 0;
    a = 1;
    v0 = ones([N1,1]);
    v1 = ones([N1-1,1]);
    v2 = ones([N1-2,1]);
    A = eye(N1) + alpha * diag(v1,1) + alpha * diag(v1,-1);
    A(1,2) = 0;
    A(N1,N1-1) = 0;
    E = zeros([N1+2,N1]);
    E(1,2) = 1;
    E(N1+2,N1-1) = 1;
    E(2:N1+1,:) = eye(N1);
    B = zeros([N1,N1+2]);
    B(1,1:6) = h4inv*[2 -9 16 -14 6 -1];
    B(end,end-5:end) = h4inv*[-1 6 -14 16 -9 2];
    vv = a * h4inv * [1 -4 6 -4 1];
    for ii = 2:N1-1
        jj = ((ii-2)) + [1:5];
        B(ii,jj) = vv;
    end

    % apply ghost cell
    B = B * E;

    % now delete first and last rows and columns since w_1 = w_N = 0
    A = A(2:N1-1,2:N1-1);
    B = B(2:N1-1,2:N1-1);

    % invert;
    D = inv(A) * B;
    
    return
    
end

% 2-d case
if (nd == 2)
    if (nargin ~= 2)
        error('cc_biharmonic with nd = 2 requires two inputs');
    end
    AR = varargin{1};
    
    % simplify
    N1 = N(1);
    N2 = N(2);
    
    % spacing
    h1 = 1 / (N1 - 1);
    h2 = 1 / (N2 - 1);

    % preallocate, including boundary nodes
    D = zeros(N1*N2);
    
    % loop over the interior points
    % 2 <= i <= N1-1
    % 2 <= j <= N2-1
    for i = 2:(N1-1)
        for j = 2:(N2-1)
            
            % simplicity
            ii      = i;
            iim1    = i-1;
            iim2    = i-2;
            iip1    = i+1;
            iip2    = i+2;
            jj      = j;
            jjm1    = j-1;
            jjm2    = j-2;
            jjp1    = j+1;
            jjp2    = j+2;
            
            % ghost cell checks
            if (iim2 == 0) 
                iim2 = 2;
            end
            if (iip2 == N1+1)
                iip2 = N1-1;
            end
            if (jjm2 == 0) 
                jjm2 = 2;
            end
            if (jjp2 == N2+1)
                jjp2 = N2-1;
            end            

            % stencil coefficients
            lij     = (jj  -1)*N1 + ii;
            lip1j   = (jj  -1)*N1 + iip1;
            lim1j   = (jj  -1)*N1 + iim1;
            lijm1   = (jjm1-1)*N1 + ii;
            lijp1   = (jjp1-1)*N1 + ii;
            lim1jm1 = (jjm1-1)*N1 + iim1;
            lip1jm1 = (jjm1-1)*N1 + iip1;
            lim1jp1 = (jjp1-1)*N1 + iim1;
            lip1jp1 = (jjp1-1)*N1 + iip1;
            lip2j   = (jj  -1)*N1 + iip2;
            lim2j   = (jj  -1)*N1 + iim2;
            lijm2   = (jjm2-1)*N1 + ii;
            lijp2   = (jjp2-1)*N1 + ii;
            
            % d^4 / dx^4
            f1 = 1 / h1^4;
            D(lij,lim2j) = D(lij,lim2j) + f1;
            D(lij,lim1j) = D(lij,lim1j) - 4*f1;
            D(lij,lij)   = D(lij,lij)   + 6*f1;
            D(lij,lip1j) = D(lij,lip1j) - 4*f1;
            D(lij,lip2j) = D(lij,lip2j) + f1;
            
            % AR^4 d^4 / dy^4
            f2 = AR^4 / h2^4;
            D(lij,lijm2) = D(lij,lijm2) + f2;
            D(lij,lijm1) = D(lij,lijm1) - 4 * f2;
            D(lij,lij)   = D(lij,lij) + 6 * f2;
            D(lij,lijp1) = D(lij,lijp1) - 4 * f2;
            D(lij,lijp2) = D(lij,lijp2) + f2;
            
            % 2 AR^2 d^4 / dx^2 dy^2
            f12 = 2 * AR^2 / h1^2 / h2^2;
            D(lij,lip1jp1) = D(lij,lip1jp1) + f12;
            D(lij,lijp1)   = D(lij,lijp1) - 2 * f12;
            D(lij,lim1jp1) = D(lij,lim1jp1) + f12;
            D(lij,lim1j)   = D(lij,lim1j) - 2 * (f12);
            D(lij,lij)     = D(lij,lij) - 2 * (-2 * f12);
            D(lij,lip1j)   = D(lij,lip1j) - 2 * (f12);
            D(lij,lip1jm1) = D(lij,lip1jm1) + f12;
            D(lij,lijm1)   = D(lij,lijm1) - 2 * f12;
            D(lij,lim1jm1) = D(lij,lim1jm1) + f12;
            
        end
    end
    
    % now we need to remove the rows and columns of D
    % the c[orrespond to boundary nodes
    rows2remove = zeros([2*N1+2*(N2-2) 1]);
    cnt = 1;
    for i = 1:N1
        
        % bottom boundary
        j = 1;
        lij = (j-1)*N1 + i;
        rows2remove(cnt) = lij;
        cnt = cnt + 1;
        
        % upper boundary
        j = N2;
        lij = (j-1)*N1 + i;
        rows2remove(cnt) = lij;
        cnt = cnt + 1;
        
    end
    
    for j = 2:N2-1
        
        % left boundary
        i = 1;
        lij = (j-1)*N1 + i;
        rows2remove(cnt) = lij;
        cnt = cnt + 1;
        
        % right boundary
        i = N1;
        lij = (j-1)*N1 + i;
        rows2remove(cnt) = lij;
        cnt = cnt + 1;        

    end
    
    D(rows2remove,:) = [];
    D(:,rows2remove) = [];
    
end

end