function [omegas2,V,X,Dx,Vx] = panel_modes_nondim_Vx_nodt(N,AR,M,mu,lambda,nmodes,target,EVP_mode)
%PANEL_MODES Compute the mode shapes and frequencies of an Euler-
%            Bernoulli panel
%
% USAGE: [D,V,x] = panel_modes(N,AR)
%
% Background:
%
% Compute the fundamental mode shapes and frequencies associated with
% the free, in vacuo, vibration of a fully-clamped Euler-Bernoulli panel.
%
% The panel is located between [0,a] x [0,b] with aspect ratio AR = a/b.
%
% The governing equation is, assuming w(x,t) = w(x) exp{i omega t},
%
%   --                                  --
%   |  d^4 w(x)     d^4 w(x)    d^4 w(x) |
% D | -------- + 2 --------- + --------- | - omega^2 rho h w(x) = 0, 
%   |   dx^4       dx^2 dy^2     dy^4    |
%   --                                  --
% where D = h^3 E / [12 (1-nu^2)] and subject to 
%
%   w(0,y) = w(a,y) = 0 and w_x(0,y) = w_x(a,y) = 0 and
%   w(x,0) = w(x,b) = 0 and w_y(x,0) = w_y(x,b) = 0.
%

%
% Get the clamped-clamped biharmonic operator on the unit interval 
% with AR parameter
%
[D,Dx,nd] = cc_operators(N,AR);

% 
% Space
%
X = zeros([N(1) N(2) 2]);
a = 1;
b = 1/AR;
[X(:,:,1) X(:,:,2)] = ndgrid(linspace(0,a,N(1)),linspace(0,b,N(2)));

%
% build RHS / LHS of EVP
%
epsilon = 1e-8;
Os      = zeros(size(D));
Is      = eye(size(D));

LHS     = D + lambda*Dx;
RHS     = mu*lambda*Is;

% 
% Compute the eigenspectrum of D
%
switch EVP_mode
    case 'eigs'
        [VV,DD] = eigs(LHS,RHS,nmodes,target);
    case 'eig'
        [VV,DD] = eig(LHS,RHS);
    otherwise
        disp('Error: Specificy ''EVP_mode'' properly!')
end
VVx = Dx*VV;
%VVx = zeros(size(VV));
%
% Sort the eigenvalues from smallest to largest
% Reshape the eigenvectors to match the grid
%
[omegas2,J] = sort(diag(DD),'ascend');
V = zeros([N(1) N(2) length(omegas2)]);
for n = 1:length(omegas2)
    V(2:N(1)-1,2:N(2)-1,n) = reshape(VV(:,J(n)),[N(1)-2 N(2)-2]);
end

%
% check x-derivative operator
%

Vx  = zeros([N(1) N(2) length(omegas2)]);
%VVx_tmp = VVx((N(1)-2)*(N(2)-2)+1:end,:);
for n = 1:length(omegas2)
    Vx(2:N(1)-1,2:N(2)-1,n) = reshape(VVx(:,J(n)),[N(1)-2 N(2)-2]);
end

end









































