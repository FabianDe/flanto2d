%% 
% Code to solve the panel response of a 2D clamped-clamped Kirchhoff 
% plate exposed to supersonic flow employing 1st order piston theory
clear all; clc; close all
clock_vector = clock;
disp('================================================')
disp([num2str(clock_vector(4)),'hr ',num2str(clock_vector(5)),'min'])
disp('================================================')
tic

%% numerical properties
N(1)        = 31;                   % number of points in streamwise direction
N(2)        = 31;                   % number of points in transverse direction
EVP_mode    = 'eigs';               % 'eig' or 'eigs'
nmodes      = 10;                   % number of modes to be stored
target      = 0;                    % target for eigs()

%% Panel properties
AR          = 1;
mu          = 0.1;

%% aerodynamic properties
M           = 10;                    % freestream Mach number
lambdas     = linspace(700,900,160);

%% solve for varying dynamic pressures
no_iter = length(lambdas)
for ii=1:length(lambdas)
    lambda    = lambdas(ii);
    
    clc
    disp([num2str(ii/no_iter*100),'%'])
    disp(['aerodynamic pressure: lambda=',num2str(lambda,'%0.2e')])

    %% compute the non-dimensional panel bending modes 
    %[lambda,V,X,Dx,Vx] = panel_modes_nondim_Vx(N,AR,M,mu,lambda,nmodes,target,EVP_mode);
    [lambda,V,X,Dx,Vx] = panel_modes_nondim_Vx_nodt(N,AR,M,mu,lambda,nmodes,target,EVP_mode);
    
    %% save spectrum
    data(ii).omega       = lambda;
    data(ii).eigvecs     = V;
    data(ii).eigvecs_dx  = Vx;
end

%% save stuff
fname = ['FLANTO2D_AR',num2str(AR,'%0.0f'),'_Nx',num2str(N(1),'%0.0f'),...
         '_Ny',num2str(N(1),'%0.0f'),'_',num2str(lambdas(1),'%0.0f'),...
         'lambdas',num2str(lambdas(end),'%0.0f'),'_mu',num2str(mu,'%0.2f'),'_M',...
         num2str(M,'%0.1f')];
save(['results/XueMei1993/',fname,'.mat'])
disp('Solution saved!')

%% plot eigenspectra
% Xue and Mei (1993): flutter onset frequencies for a/b=1
lambda_flut_lit = 840;

figure(2), clf;
set(gcf,'Position',[50 0 800 300])
aplot(1) = subplot(1,2,1); hold on
aplot(2) = subplot(1,2,2); hold on
for ii=1:no_iter
    plot(aplot(1),lambdas(ii),real(data(ii).omega),'k.','MarkerSize',10)
    plot(aplot(2),lambdas(ii),imag(data(ii).omega),'k.','MarkerSize',10)
end
plot(lambda_flut_lit,0,'ro','MarkerSize',10)
xlim([lambdas(1),lambdas(end)])
xl=xlabel(aplot(1),'$\lambda$'); set(xl,'Interpreter','Latex','FontSize',24)
xl=xlabel(aplot(2),'$\lambda$'); set(xl,'Interpreter','Latex','FontSize',24)
yl=ylabel(aplot(1),'$\mathcal{R}(\omega)$'); set(yl,'Interpreter','Latex','FontSize',24)
yl=ylabel(aplot(2),'$\mathcal{I}(\omega)$'); set(yl,'Interpreter','Latex','FontSize',24)
ylim(aplot(1),[0,1.1*max(real(data(1).omega))])

set(gcf,'PaperPositionMode','Auto')
print(gcf,['results/XueMei1993/',fname,'.jpg'],'-djpeg90')
print(gcf,['results/XueMei1993/',fname,'.eps'],'-depsc')
savefig(gcf,['results/XueMei1993/',fname,'.fig'])

%% plot eigenspectrum
i_lambda    = 10;
ncontours   = 20;
figure(3), clf;
set(gcf,'Position',[800 400 1200 500])
asub(1) = subplot(1,3,1); hold on
for i=1:length(data(i_lambda).omega)
    text(real(data(i_lambda).omega(i)),imag(data(i_lambda).omega(i)),num2str(i),'color','k')
end
xl=xlabel('$\mathcal{R}(\omega)$'); set(xl,'Interpreter','Latex','FontSize',24)
yl=ylabel('$\mathcal{I}(\omega)$'); set(yl,'Interpreter','Latex','FontSize',24)
xlim([0,100])
ylim([-100,100])
EVnum = 1;
while(1)
    disp('enter eigenvalue number:')
    EVnum_old = EVnum;
    EVnum = input(['Which number to plot? (<0> to abort)']);
    if(EVnum==0)
        break
    end
    asub(2) = subplot(1,3,2);
    contourf(X(:,:,1),X(:,:,2),real(data(i_lambda).eigvecs(:,:,EVnum)),ncontours);
    leg=legend('$\hat{W}$');
    set(leg,'Interpreter','Latex','FontSize',22)
    asub(3) = subplot(1,3,3);
    contourf(X(:,:,1),X(:,:,2),real(data(i_lambda).eigvecs_dx(:,:,EVnum)),ncontours);
    leg=legend('$\frac{\partial \hat{W}}{\partial x}$');
    set(leg,'Interpreter','Latex','FontSize',22)
    xl=xlabel('$x$'); set(xl,'Interpreter','Latex','FontSize',24)
    yl=ylabel('$y$'); set(yl,'Interpreter','Latex','FontSize',24)
end
EVnum = EVnum_old;