function [D,V,X] = panel_modes(N,AR,nmodes,target,EVP_mode)
%PANEL_MODES Compute the mode shapes and frequencies of an Euler-
%            Bernoulli panel
%
% USAGE: [D,V,x] = panel_modes(N,AR)
%
% Background:
%
% Compute the fundamental mode shapes and frequencies associated with
% the free, in vacuo, vibration of a fully-clamped Euler-Bernoulli panel.
%
% The panel is located between [0,a] x [0,b] with aspect ratio AR = a/b.
%
% The governing equation is, assuming w(x,t) = w(x) exp{i omega t},
%
%   --                                  --
%   |  d^4 w(x)     d^4 w(x)    d^4 w(x) |
% D | -------- + 2 --------- + --------- | - omega^2 rho h w(x) = 0, 
%   |   dx^4       dx^2 dy^2     dy^4    |
%   --                                  --
% where D = h^3 E / [12 (1-nu^2)] and subject to 
%
%   w(0,y) = w(a,y) = 0 and w_x(0,y) = w_x(a,y) = 0 and
%   w(x,0) = w(x,b) = 0 and w_y(x,0) = w_y(x,b) = 0.
%

%
% Get the clamped-clamped biharmonic operator on the unit interval 
% with AR parameter
%
[D,nd] = cc_biharmonic(N,AR);
if (nd ~= 2)
    error('nd != 2 in panel_modes()');
end

% 
% Space
%
X = zeros([N(1) N(2) 2]);
a = 1;
b = 1;
if AR ~= 0
    b = 1 / AR;
end
[X(:,:,1) X(:,:,2)] = ndgrid(linspace(0,a,N(1)),linspace(0,b,N(2)));

% 
% Compute the eigenspectrum of D
%
switch EVP_mode
    case 'eigs'
        [VV,DD] = eigs(D,nmodes,target);
    case 'eig'
        [VV,DD] = eig(D);
    otherwise
        disp('Error: Specificy ''EVP_mode'' properly!')
end

%
% Sort the eigenvalues from smallest to largest
% Reshape the eigenvectors to match the grid
%
[D,J] = sort(diag(DD),'ascend');
V = zeros([N(1) N(2) length(D)]);
for n = 1:length(D)
    V(2:N(1)-1,2:N(2)-1,n) = reshape(VV(:,J(n)),[N(1)-2 N(2)-2]);
end


end

