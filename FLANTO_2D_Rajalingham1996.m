clear all; clc; %close all
%% 
%  Matlab script for computing initial velocity of the panel in the
%  AFPTech-Leidos simulations adopted from Shideler, Dixon, and Shore 
%  (NASA-TN D-3498, 1966) 

%% numerical properties
N(1)        = 31;                   % number of points in streamwise direction
N(2)        = 31;                   % number of points in transverse direction
nmodes      = 20;                   % number of modes to be stored
target      = 0;                    % target for eigs()
EVP_mode    = 'eigs';

%% Panel properties
a           = 1;                % streamwise length, m
b           = 1;                % transverse length, m
h           = 0.00001;              % thickness, m
E           = 2.1e11;              % Young's modulus, Pa
rho_m       = 2780;                 % density, kg / m^3
nu          = 0.3;                  % Poisson's ratio

%% aerodynamic properties
rho_a       = 1.225;                % freestream density

%% derived quantities
AR          = a / b;                % panel aspect ratio
Dstiff      = E*h^3/(12*(1-nu^2));  % bending stiffness

%% compute the non-dimensional panel bending modes
[omegas,V,X] = panel_modes(N,AR,nmodes,target,EVP_mode);

%% dimensionalize
%omega       = sqrt(lambda * Dstiff / (rho_m * h * a^4));
omega       = omegas;
f           = omega / (2 * pi);
X           = X * a;

%% plot eigenspectrum
while(1)
ncontours = 20;
figure(2), clf;
set(gcf,'Position',[50 0 1200 500])
asub(1) = subplot(1,2,1); hold on
for i=1:length(omega)
    text(real(omega(i)),imag(omega(i)),num2str(i),'color','k')
end
xl=xlabel('$\mathcal{R}(\omega)$'); set(xl,'Interpreter','Latex','FontSize',24)
yl=ylabel('$\mathcal{I}(\omega)$'); set(yl,'Interpreter','Latex','FontSize',24)
xlim([0,max(real(omega))])
%ylim([min(imag(omega)),max(imag(omega))]*1.1)

EVnum = 1;
while(1)
    disp('enter eigenvalue number:')
    EVnum_old = EVnum;
    EVnum = input(['Which number to plot? (<0> to abort)']);
    if(EVnum==0)
        break
    end
    disp(['omega(',num2str(EVnum),')=',num2str(omega(EVnum))])
    asub(2) = subplot(1,2,2);
    contourf(X(:,:,1),X(:,:,2),real(V(:,:,EVnum)),ncontours); hold on
    %contour(X(:,:,1),X(:,:,2),real(V(:,:,EVnum)),ncontours);
    %str = sprintf('Frequency $f =$ %.3e Hz',f(EVnum));
    %title(str,'Interpreter','latex');
    str2 = ['$\omega a^2 \sqrt(\frac{\rho_m h}{D}) = $',num2str(omega(EVnum)*a^2*sqrt(rho_m*h/Dstiff),'%0.4f')];
    title(str2,'Interpreter','latex');	
    leg=legend('$\hat{W}$');
    set(leg,'Interpreter','Latex','FontSize',22)
    xl=xlabel('$x$'); set(xl,'Interpreter','Latex','FontSize',24)
    yl=ylabel('$y$'); set(yl,'Interpreter','Latex','FontSize',24)
end
EVnum = EVnum_old;
break
end

%% first harmonic wave

freqs = [ 51, 35.9159, 73.1342, 107.7620, 130.7832, 131.4154, 163.9998, 163.9998, 208.5881, 208.5881;...
          81, 35.9581, 73.2921, 108.0379, 131.2677, 131.8950, 164.6067, 164.6067, 209.7618, 209.7618;...
         101, 35.9678, 73.3286, 108.1020, 131.3802, 132.0063, 164.7480, 164.7480, 210.0347, 210.0347;...
         121, 35.9731, 73.3485, 108.1369, 131.4414, 132.0668, 164.8249, 164.8249, 210.1833, 210.1833;...
         151, 35.9775, 73.3648, 108.1656, 131.4915, 132.1165, 164.8880, 164.8880, 210.3050, 210.3050;...
         181, 35.9798, 73.3737, 108.1811, 131.5188, 132.1434, 164.9223, 164.9223, 210.3712, 210.3712];

exact = [35.9866,73.40,108.22,131.64,132.18,164.99;... %Leissa
         35.99,73.41,108.27,131.64,132.25,165.15;...  % Leissa
         35.99896,73.40536,108.23591,131.90213,131.90213,165.02304]; % Rajalingham et al.




for i=1:length(exact)
    err_mode1 = abs((freqs(:,2)-35.9798)/35.9798);
    err_mode2 = abs((freqs(:,3)-73.3737)/73.3737);
    err_mode3 = abs((freqs(:,4)-108.1811)/108.1811);
    err_mode4 = abs((freqs(:,5)-131.5188)/131.5188);
    err_mode5 = abs((freqs(:,6)-132.1434)/132.1434);
    err_mode6 = abs((freqs(:,7)-164.9223)/164.9223);
    
    err_mode1_lit = abs((freqs(:,2)-35.985)/35.985);    % Rajalingham et al
    err_mode2_lit = abs((freqs(:,3)-73.40536)/73.40536);
    err_mode3_lit = abs((freqs(:,4)-108.23591)/108.23591);
    err_mode4_lit = abs((freqs(:,5)-131.581)/131.581);
    err_mode5_lit = abs((freqs(:,6)-132.207)/132.207);
    err_mode6_lit = abs((freqs(:,7)-165.02304)/165.02304);
end


figure(3);clf
semilogy(freqs(:,1),err_mode1_lit,'LineWidth',2); hold on
semilogy(freqs(:,1),err_mode2_lit,'LineWidth',2)
semilogy(freqs(:,1),err_mode3_lit,'LineWidth',2)
semilogy(freqs(:,1),err_mode4_lit,'LineWidth',2)
semilogy(freqs(:,1),err_mode5_lit,'LineWidth',2)
xlim([50,160])
ylim([1e-4,1e-2])
set(gca,'FontSize',16)
leg=legend('mode 1','mode 2','mode 3','mode 4','mode 5');
set(leg,'Interpreter','Latex','FontSize',18)
xlabel('$N$','Interpreter','Latex','FontSize',24)
ylabel('$\vert \epsilon \vert=\vert \frac{\omega_{(N)} - \omega_{Raj}}{\omega_{Raj}} \vert$','Interpreter','Latex','FontSize',30)
set(gcf,'Position',[50 0 400 400])
print(gcf,'results/Rajalingham/err_plot','-djpeg90')
print(gcf,'results/Rajalingham/err_plot','-depsc')
savefig(gcf,'results/Rajalingham/err_plot')

figure(4),clf
semilogy(freqs(:,1),err_mode1,'LineWidth',2); hold on
semilogy(freqs(:,1),err_mode2,'LineWidth',2)
semilogy(freqs(:,1),err_mode3,'LineWidth',2)
semilogy(freqs(:,1),err_mode4,'LineWidth',2)
semilogy(freqs(:,1),err_mode5,'LineWidth',2)
xlim([50,160])
ylim([1e-4,1e-2])
set(gca,'FontSize',16)
leg=legend('mode 1','mode 2','mode 3','mode 4','mode 5');
set(leg,'Interpreter','Latex','FontSize',18)
xlabel('$N$','Interpreter','Latex','FontSize',24)
ylabel('$\vert \epsilon \vert=\vert \frac{\omega_{(N)} - \omega_{(N=180)}}{\omega_{(N=180)}} \vert$','Interpreter','Latex','FontSize',30)
set(gcf,'Position',[450 0 400 400])
print(gcf,'results/Rajalingham/grid_convergence','-djpeg90')
print(gcf,'results/Rajalingham/grid_convergence','-depsc')
savefig(gcf,'results/Rajalingham/grid_convergence')


%% plot the first few modes
ncontours   = 20;
labelSize   = 24;
for ii=1:4
    figure(5),clf;
    contourf(X(:,:,1),X(:,:,2),squeeze(real(V(:,:,ii))),ncontours); hold on
    %contour(X(:,:,1),X(:,:,2),real(V(:,:,ii)),ncontours) ; hold off
    pbaspect([1 1 1])
    set(gca,'FontSize',16,'XTick',[0,1],'YTick',[0,1])
    
    leg=legend(['mode $',num2str(ii),'$']); set(leg,'FontSize',20,'Interpreter','Latex')
    xlabel('x','FontSize',labelSize); ylabel('y','FontSize',labelSize,'Rotation',0)
    set(gcf,'Position',[450 600 400 400])
    print(gcf,['results/Rajalingham/eigenfunction_mode',num2str(ii)],'-djpeg90')
    print(gcf,['results/Rajalingham/eigenfunction_mode',num2str(ii)],'-depsc')
    savefig(gcf,['results/Rajalingham/eigenfunction_mode',num2str(ii)])
end






















